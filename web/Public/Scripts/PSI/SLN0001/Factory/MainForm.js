/**
 * 工厂 - 主界面
 * 
 * @author 艾格林门信息服务（大连）有限公司
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Factory.MainForm", {
  extend: "PSI.AFX.Form.MainForm",

  config: {
    permission: null
  },

  /**
   * @override
   */
  initComponent() {
    var me = this;

    PCL.apply(me, {
      items: [{
        tbar: me.getToolbarCmp(),
        id: "panelQueryCmp",
        region: "north",
        height: 95,
        border: 0,
        collapsible: true,
        collapseMode: "mini",
        header: false,
        layout: {
          type: "table",
          columns: 4
        },
        bodyCls: "PSI-Query-Panel",
        items: me.getQueryCmp()
      }, {
        region: "center",
        xtype: "container",
        layout: "border",
        border: 0,
        items: [{
          region: "center",
          xtype: "panel",
          layout: "fit",
          border: 0,
          items: [me.getMainGrid()]
        }, {
          id: "panelCategory",
          xtype: "panel",
          region: "west",
          layout: "fit",
          width: 370,
          split: true,
          collapsible: true,
          header: false,
          border: 0,
          items: [me.getCategoryGrid()]
        }]
      }]
    });

    me.callParent(arguments);

    me.__queryEditNameList = ["editQueryCode", "editQueryName",
      "editQueryAddress", "editQueryContact", "editQueryMobile",
      "editQueryTel"];

    me._dfFactoryInfo = PCL.getCmp("PSI_SLN0001_Factory_MainForm_dfFactoryInfo");

    me.freshCategoryGrid();
  },

  getToolbarCmp() {
    var me = this;

    return [{
      iconCls: "PSI-tb-new",
      text: "新建工厂分类",
      hidden: me.getPermission().addCategory == "0",
      handler: me.onAddCategory,
      scope: me
    }, {
      text: "编辑工厂分类",
      hidden: me.getPermission().editCategory == "0",
      handler: me.onEditCategory,
      scope: me
    }, {
      text: "删除工厂分类",
      hidden: me.getPermission().deleteCategory == "0",
      handler: me.onDeleteCategory,
      scope: me
    }, {
      hidden: me.getPermission().deleteCategory == "0",
      xtype: "tbseparator"
    }, {
      iconCls: "PSI-tb-new-entity",
      text: "新建工厂",
      hidden: me.getPermission().add == "0",
      handler: me.onAddFactory,
      scope: me
    }, {
      text: "编辑工厂",
      hidden: me.getPermission().edit == "0",
      handler: me.onEditFactory,
      scope: me
    }, {
      text: "删除工厂",
      hidden: me.getPermission().del == "0",
      handler: me.onDeleteFactory,
      scope: me
    }, {
      hidden: me.getPermission().del == "0",
      xtype: "tbseparator"
    }, {
      iconCls: "PSI-tb-help",
      text: "指南",
      handler() {
        me.focus();
        window.open(me.URL("Home/Help/index?t=factory"));
      }
    }, "-", {
      iconCls: "PSI-tb-close",
      text: "关闭",
      handler() {
        me.closeWindow();
      }
    }, {
      // 空容器，只是为了撑高工具栏
      xtype: "container", height: 28,
      items: []
    }].concat(me.getShortcutCmp());
  },

  /**
   * 快捷访问
   */
  getShortcutCmp() {
    return ["->",
      {
        cls: "PSI-Shortcut-Cmp",
        labelWidth: 0,
        emptyText: "快捷访问",
        xtype: "psi_mainmenushortcutfield",
        width: 90
      }
    ];
  },

  getQueryCmp() {
    var me = this;

    return [{
      id: "editQueryCode",
      labelWidth: 70,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "工厂编码",
      margin: "5, 0, 0, 0",
      xtype: "textfield",
      listeners: {
        specialkey: {
          fn: me.onQueryEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: "editQueryName",
      labelWidth: 70,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "工厂名称",
      margin: "5, 0, 0, 0",
      xtype: "textfield",
      listeners: {
        specialkey: {
          fn: me.onQueryEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: "editQueryAddress",
      labelWidth: 70,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "地址",
      margin: "5, 0, 0, 0",
      xtype: "textfield",
      listeners: {
        specialkey: {
          fn: me.onQueryEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: "editQueryContact",
      labelWidth: 70,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "联系人",
      margin: "5, 0, 0, 0",
      xtype: "textfield",
      listeners: {
        specialkey: {
          fn: me.onQueryEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: "editQueryMobile",
      labelWidth: 70,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "手机",
      margin: "5, 0, 0, 0",
      xtype: "textfield",
      listeners: {
        specialkey: {
          fn: me.onQueryEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: "editQueryTel",
      labelWidth: 70,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "固话",
      margin: "5, 0, 0, 0",
      xtype: "textfield",
      listeners: {
        specialkey: {
          fn: me.onQueryEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: "editQueryRecordStatus",
      xtype: "combo",
      queryMode: "local",
      editable: false,
      valueField: "id",
      labelWidth: 70,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "状态",
      margin: "5, 0, 0, 0",
      store: PCL.create("PCL.data.ArrayStore", {
        fields: ["id", "text"],
        data: [[-1, "全部"], [1000, "启用"], [0, "停用"]]
      }),
      value: -1
    }, {
      xtype: "container",
      items: [{
        xtype: "button",
        text: "查询",
        width: 100,
        height: 26,
        margin: "5, 0, 0, 20",
        handler: me.onQuery,
        scope: me
      }, {
        xtype: "button",
        text: "清空查询条件",
        width: 100,
        height: 26,
        margin: "5, 0, 0, 15",
        handler: me.onClearQuery,
        scope: me
      }, {
        xtype: "button",
        text: "隐藏工具栏",
        width: 130,
        height: 26,
        iconCls: "PSI-button-hide",
        margin: "5 0 0 10",
        handler() {
          PCL.getCmp("panelQueryCmp").collapse();
        },
        scope: me
      }]
    }];
  },

  getCategoryGrid() {
    var me = this;
    if (me.__categoryGrid) {
      return me.__categoryGrid;
    }

    var modelName = "PSIFactoryCategory";
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "code", "name", {
        name: "cnt",
        type: "int"
      }]
    });

    me.__categoryGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-FC",
      viewConfig: {
        enableTextSelection: true
      },
      header: {
        height: 30,
        title: me.formatGridHeaderTitle("工厂分类")
      },
      tools: [{
        type: "close",
        handler() {
          PCL.getCmp("panelCategory").collapse();
        }
      }],
      features: [{
        ftype: "summary"
      }],
      columnLines: true,
      columns: [{
        header: "分类编码",
        dataIndex: "code",
        width: 80,
        menuDisabled: true,
        sortable: false
      }, {
        header: "工厂分类",
        dataIndex: "name",
        width: 160,
        menuDisabled: true,
        sortable: false,
        summaryRenderer() {
          return "工厂个数合计";
        }
      }, {
        header: "工厂个数",
        dataIndex: "cnt",
        width: 100,
        menuDisabled: true,
        sortable: false,
        summaryType: "sum",
        align: "right"
      }],
      store: PCL.create("PCL.data.Store", {
        model: modelName,
        autoLoad: false,
        data: []
      }),
      listeners: {
        select: {
          fn: me.onCategoryGridSelect,
          scope: me
        },
        itemdblclick: {
          fn: me.onEditCategory,
          scope: me
        }
      }
    });

    return me.__categoryGrid;
  },

  getMainGrid() {
    var me = this;
    if (me.__mainGrid) {
      return me.__mainGrid;
    }

    var modelName = "PSIFactory";
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "code", "name", "contact01", "tel01",
        "mobile01", "contact02", "tel02", "mobile02",
        "categoryId", "initPayables", "initPayablesDT",
        "address", "bankName", "bankAccount", "tax", "fax",
        "note", "dataOrg", "recordStatus"]
    });

    var store = PCL.create("PCL.data.Store", {
      autoLoad: false,
      model: modelName,
      data: [],
      pageSize: 20,
      proxy: {
        type: "ajax",
        actionMethods: {
          read: "POST"
        },
        url: me.URL("SLN0001/Factory/factoryList"),
        reader: {
          root: 'dataList',
          totalProperty: 'totalCount'
        }
      },
      listeners: {
        beforeload: {
          fn() {
            store.proxy.extraParams = me.getQueryParam();
          },
          scope: me
        },
        load: {
          fn(e, records, successful) {
            if (successful) {
              me.refreshCategoryCount();
              me.gotoMainGridRecord(me.__lastId);
            }
          },
          scope: me
        }
      }
    });

    me.__mainGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-LC",
      viewConfig: {
        enableTextSelection: true
      },
      columnLines: true,
      columns: {
        defaults: {
          menuDisabled: true,
          sortable: false
        },
        items: [PCL.create("PCL.grid.RowNumberer", {
          text: "#",
          width: 40
        }), {
          header: "工厂编码",
          locked: true,
          dataIndex: "code",
          renderer(value, metaData, record) {
            if (parseInt(record.get("recordStatus")) == 1000) {
              return value;
            } else {
              return `<span class="PSI-record-disabled">${value}</span>`;
            }
          }
        }, {
          header: "工厂名称",
          locked: true,
          dataIndex: "name",
          width: 300,
          renderer(value, metaData, record) {
            if (parseInt(record.get("recordStatus")) == 1000) {
              return value;
            } else {
              return `<span class="PSI-record-disabled">${value}</span>`;
            }
          }
        }, {
          header: "地址",
          dataIndex: "address",
          width: 300
        }, {
          header: "联系人",
          dataIndex: "contact01"
        }, {
          header: "手机",
          dataIndex: "mobile01"
        }, {
          header: "固话",
          dataIndex: "tel01"
        }, {
          header: "备用联系人",
          dataIndex: "contact02"
        }, {
          header: "备用联系人手机",
          dataIndex: "mobile02",
          width: 150
        }, {
          header: "备用联系人固话",
          dataIndex: "tel02",
          width: 150
        }, {
          header: "开户行",
          dataIndex: "bankName"
        }, {
          header: "开户行账号",
          dataIndex: "bankAccount"
        }, {
          header: "税号",
          dataIndex: "tax"
        }, {
          header: "传真",
          dataIndex: "fax"
        }, {
          header: "应付期初余额",
          dataIndex: "initPayables",
          align: "right",
          xtype: "numbercolumn",
          width: 150
        }, {
          header: "应付期初余额日期",
          dataIndex: "initPayablesDT",
          width: 150
        }, {
          header: "备注",
          dataIndex: "note",
          width: 400
        }, {
          header: "数据域",
          dataIndex: "dataOrg"
        }, {
          header: "状态",
          dataIndex: "recordStatus",
          renderer(value) {
            if (parseInt(value) == 1000) {
              return "启用";
            } else {
              return "<span style='color:red'>停用</span>";
            }
          }
        }]
      },
      store: store,
      tbar: ["", {
        xtype: "displayfield",
        id: "PSI_SLN0001_Factory_MainForm_dfFactoryInfo",
        value: "工厂列表",
      }, "->", {
          id: "pagingToolbar",
          border: 0,
          xtype: "pagingtoolbar",
          store: store
        }, "-", {
          xtype: "displayfield",
          value: "每页显示"
        }, {
          id: "comboCountPerPage",
          xtype: "combobox",
          editable: false,
          width: 60,
          store: PCL.create("PCL.data.ArrayStore", {
            fields: ["text"],
            data: [["20"], ["50"], ["100"], ["300"],
            ["1000"]]
          }),
          value: 20,
          listeners: {
            change: {
              fn() {
                store.pageSize = PCL.getCmp("comboCountPerPage").getValue();
                store.currentPage = 1;
                PCL.getCmp("pagingToolbar").doRefresh();
              },
              scope: me
            }
          }
        }, {
          xtype: "displayfield",
          value: "条记录"
        }],
      listeners: {
        itemdblclick: {
          fn: me.onEditFactory,
          scope: me
        }
      }
    });

    return me.__mainGrid;
  },

  onAddCategory() {
    var me = this;

    var form = PCL.create("PSI.SLN0001.Factory.CategoryEditForm", {
      parentForm: me
    });

    form.show();
  },

  onEditCategory() {
    var me = this;
    if (me.getPermission().editCategory == "0") {
      return;
    }

    var item = me.getCategoryGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("请选择要编辑的工厂分类");
      return;
    }

    var category = item[0];

    var form = PCL.create("PSI.SLN0001.Factory.CategoryEditForm", {
      parentForm: me,
      entity: category
    });

    form.show();
  },

  onDeleteCategory() {
    var me = this;

    var item = me.getCategoryGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      PSI.MsgBox.showInfo("请选择要删除的工厂分类");
      return;
    }

    var category = item[0];
    var info = "请确认是否删除工厂分类: <span style='color:red'>"
      + category.get("name") + "</span>";

    var store = me.getCategoryGrid().getStore();
    var index = store.findExact("id", category.get("id"));
    index--;
    var preIndex = null;
    var preItem = store.getAt(index);
    if (preItem) {
      preIndex = preItem.get("id");
    }

    me.confirm(info, () => {
      var el = PCL.getBody();
      el.mask("正在删除中...");
      me.ajax({
        url: me.URL("SLN0001/Factory/deleteCategory"),
        params: {
          id: category.get("id")
        },
        callback(options, success, response) {
          el.unmask();

          if (success) {
            var data = me.decodeJSON(response.responseText);
            if (data.success) {
              me.tip("成功完成删除操作");
              me.freshCategoryGrid(preIndex);
            } else {
              me.showInfo(data.msg);
            }
          }
        }
      });
    });
  },

  freshCategoryGrid(id) {
    var me = this;
    var grid = me.getCategoryGrid();
    var el = grid.getEl() || PCL.getBody();
    el.mask(PSI.Const.LOADING);
    me.ajax({
      url: me.URL("SLN0001/Factory/categoryList"),
      params: me.getQueryParam(),
      callback(options, success, response) {
        var store = grid.getStore();

        store.removeAll();

        if (success) {
          var data = me.decodeJSON(response.responseText);
          store.add(data);

          if (id) {
            var r = store.findExact("id", id);
            if (r != -1) {
              grid.getSelectionModel().select(r);
            }
          } else {
            grid.getSelectionModel().select(0);
          }
        }

        el.unmask();
      }
    });
  },

  freshMainGrid(id) {
    var me = this;

    var item = me.getCategoryGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me._dfFactoryInfo.setValue("工厂列表");
      return;
    }

    var category = item[0];

    const info = `<span class='PSI-title-keyword'>${category.get("name")}</span> - 工厂列表`;
    me._dfFactoryInfo.setValue(info);

    me.__lastId = id;
    PCL.getCmp("pagingToolbar").doRefresh()
  },

  onCategoryGridSelect() {
    var me = this;
    me.getMainGrid().getStore().currentPage = 1;
    me.freshMainGrid();
  },

  onAddFactory() {
    var me = this;

    if (me.getCategoryGrid().getStore().getCount() == 0) {
      me.showInfo("没有工厂分类，请先新建工厂分类");
      return;
    }

    var form = PCL.create("PSI.SLN0001.Factory.FactoryEditForm", {
      parentForm: me
    });

    form.show();
  },

  onEditFactory() {
    var me = this;
    if (me.getPermission().edit == "0") {
      return;
    }

    var item = me.getCategoryGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择工厂分类");
      return;
    }
    var category = item[0];

    var item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("请选择要编辑的工厂");
      return;
    }

    var factory = item[0];
    factory.set("categoryId", category.get("id"));
    var form = PCL.create("PSI.SLN0001.Factory.FactoryEditForm", {
      parentForm: me,
      entity: factory
    });

    form.show();
  },

  onDeleteFactory() {
    var me = this;
    var item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("请选择要删除的工厂");
      return;
    }

    var factory = item[0];

    var store = me.getMainGrid().getStore();
    var index = store.findExact("id", factory.get("id"));
    index--;
    var preIndex = null;
    var preItem = store.getAt(index);
    if (preItem) {
      preIndex = preItem.get("id");
    }

    var info = "请确认是否删除工厂: <span style='color:red'>" + factory.get("name")
      + "</span>";
    me.confirm(info, () => {
      var el = PCL.getBody();
      el.mask("正在删除中...");
      me.ajax({
        url: me.URL("SLN0001/Factory/deleteFactory"),
        params: {
          id: factory.get("id")
        },
        callback(options, success, response) {
          el.unmask();

          if (success) {
            var data = me.decodeJSON(response.responseText);
            if (data.success) {
              me.tip("成功完成删除操作");
              me.freshMainGrid(preIndex);
            } else {
              me.showInfo(data.msg);
            }
          }
        }

      });
    });
  },

  gotoMainGridRecord(id) {
    var me = this;
    var grid = me.getMainGrid();
    var store = grid.getStore();
    if (id) {
      var r = store.findExact("id", id);
      if (r != -1) {
        grid.getSelectionModel().select(r);
      } else {
        grid.getSelectionModel().select(0);
      }
    } else {
      grid.getSelectionModel().select(0);
    }
  },

  refreshCategoryCount() {
    var me = this;
    var item = me.getCategoryGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }

    var category = item[0];
    category.set("cnt", me.getMainGrid().getStore().getTotalCount());
    me.getCategoryGrid().getStore().commitChanges();
  },

  onQueryEditSpecialKey(field, e) {
    if (e.getKey() === e.ENTER) {
      var me = this;
      var id = field.getId();
      for (var i = 0; i < me.__queryEditNameList.length - 1; i++) {
        var editorId = me.__queryEditNameList[i];
        if (id === editorId) {
          var edit = PCL.getCmp(me.__queryEditNameList[i + 1]);
          edit.focus();
          edit.setValue(edit.getValue());
        }
      }
    }
  },

  onLastQueryEditSpecialKey(field, e) {
    var me = this;

    if (e.getKey() === e.ENTER) {
      me.onQuery();
    }
  },

  getQueryParam() {
    var me = this;
    var item = me.getCategoryGrid().getSelectionModel().getSelection();
    var categoryId;
    if (item == null || item.length != 1) {
      categoryId = null;
    } else {
      categoryId = item[0].get("id");
    }

    var result = {
      categoryId: categoryId
    };

    var code = PCL.getCmp("editQueryCode").getValue();
    if (code) {
      result.code = code;
    }

    var address = PCL.getCmp("editQueryAddress").getValue();
    if (address) {
      result.address = address;
    }

    var name = PCL.getCmp("editQueryName").getValue();
    if (name) {
      result.name = name;
    }

    var contact = PCL.getCmp("editQueryContact").getValue();
    if (contact) {
      result.contact = contact;
    }

    var mobile = PCL.getCmp("editQueryMobile").getValue();
    if (mobile) {
      result.mobile = mobile;
    }

    var tel = PCL.getCmp("editQueryTel").getValue();
    if (tel) {
      result.tel = tel;
    }

    result.recordStatus = PCL.getCmp("editQueryRecordStatus").getValue();

    return result;
  },

  onQuery() {
    var me = this;

    me.getMainGrid().getStore().removeAll();
    me.freshCategoryGrid();
  },

  onClearQuery() {
    var me = this;

    var nameList = me.__queryEditNameList;
    for (var i = 0; i < nameList.length; i++) {
      var name = nameList[i];
      var edit = PCL.getCmp(name);
      if (edit) {
        edit.setValue(null);
      }
    }

    PCL.getCmp("editQueryRecordStatus").setValue(-1);

    me.onQuery();
  }
});
